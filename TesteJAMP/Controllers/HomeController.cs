﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TesteJAMP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Aplicação teste Mussa para Neppo 2018.";
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "João Mussa. Telefone: (34) 99666-9573";

            return View();
        }
    }
}