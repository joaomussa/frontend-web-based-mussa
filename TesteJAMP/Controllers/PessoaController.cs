﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesteJAMP.Models;

namespace TesteJAMP.Controllers
{
    public class PessoaController : Controller
    {
        private PessoaContextDB2 db = new PessoaContextDB2();
        

        // GET: Pessoa
        public ActionResult Index()
        {
            return View(db.PessoaBO.ToList());
        }

        // GET: Relatorio
        public ActionResult Relatorio()
        {
            var pessoas = db.PessoaBO.ToList();
            RelatorioBO relatorio = new RelatorioBO(pessoas);
            return View(relatorio);
        }
        
        // GET: Pessoa/Details/5
        public ActionResult Details(string rg)
        {
            if (rg == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoaFound = db.PessoaBO.Find(rg);
            if (pessoaFound == null)
            {
                return HttpNotFound();
            }
            return View(pessoaFound);
        }

        // GET: Pessoa/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pessoa/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Nome,DataNascimento,Rg,Sexo,Endereco")] Pessoa p)
        {
            if (ModelState.IsValid)
            {
                Pessoa pessoaFound = db.PessoaBO.Find(p.Rg);
                if (pessoaFound != null)
                {
                    return RedirectToAction("RGExistente", "Custom");
                }

                db.PessoaBO.Add(p);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(p);
        }

        // GET: Pessoa/Edit/5
        public ActionResult Edit(string rg)
        {
            if (rg == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoaFound = db.PessoaBO.Find(rg);
            if (pessoaFound == null)
            {
                return HttpNotFound();
            }
            return View(pessoaFound);
        }
        

        // POST: Pessoa/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Nome,DataNascimento,Rg,Sexo,Endereco")] Pessoa p)
        {
            if (ModelState.IsValid)
            {
                db.Entry(p).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(p);
        }

        // GET: Pessoa/Delete/5
        public ActionResult Delete(string rg)
        {
            if (rg == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoaFound = db.PessoaBO.Find(rg);
            if (pessoaFound == null)
            {
                return HttpNotFound();
            }
            return View(pessoaFound);
        }

        // POST: Pessoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string rg)
        {
            Pessoa pessoaFound = db.PessoaBO.Find(rg);
            db.PessoaBO.Remove(pessoaFound);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
