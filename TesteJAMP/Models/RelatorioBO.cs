﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TesteJAMP.Models;

namespace TesteJAMP.Models
{
    public class RelatorioBO
    {
        public double perc0_9 { get; set; }
        public double perc10_19 { get; set; }
        public double perc20_29 { get; set; }
        public double perc30_39 { get; set; }
        public double perc40 { get; set; }
        public double percMasc { get; set; }
        public double percFem { get; set; }
        public int qntMasc { get; set; }
        public int qntFem { get; set; }
        public int qntPessoas { get; set; }

        public RelatorioBO (List<Pessoa> pessoas)
        {
            perc0_9 = System.Math.Round((double)((pessoas.FindAll(x => GetAge(x.DataNascimento) >= 0 && GetAge(x.DataNascimento) <= 9).Count)) / pessoas.Count * 100,2);
            perc10_19 = System.Math.Round((double)((pessoas.FindAll(x => GetAge(x.DataNascimento) >= 10 && GetAge(x.DataNascimento) <= 19).Count)) / pessoas.Count * 100, 2);
            perc20_29 = System.Math.Round((double)((pessoas.FindAll(x => GetAge(x.DataNascimento) >= 20 && GetAge(x.DataNascimento) <= 29).Count)) / pessoas.Count * 100, 2);
            perc30_39 = System.Math.Round((double)((pessoas.FindAll(x => GetAge(x.DataNascimento) >= 30 && GetAge(x.DataNascimento) <= 39).Count)) / pessoas.Count * 100, 2);
            perc40 = System.Math.Round((double)((pessoas.FindAll(x => GetAge(x.DataNascimento) >= 40).Count)) / pessoas.Count * 100, 2);
            percMasc = System.Math.Round((double)((pessoas.FindAll(x => x.Sexo == "Masculino").Count)) / pessoas.Count * 100, 2);
            percFem = System.Math.Round((double)((pessoas.FindAll(x => x.Sexo == "Feminino").Count)) / pessoas.Count * 100, 2);
            qntMasc = pessoas.FindAll(x => x.Sexo == "Masculino").Count;
            qntFem = pessoas.FindAll(x => x.Sexo == "Feminino").Count;
            qntPessoas = pessoas.Count;
        }

        public static Int32 GetAge(string Birthday)
        {
            //Mussa - Ignorando anos bissextos e aniversário passado.
            DateTime dateOfBirth = DateTime.Parse(Birthday);
            var today = DateTime.Today;

            var age = today.Year - dateOfBirth.Year;

            return age;
        }



    }
}