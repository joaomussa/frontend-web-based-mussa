﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TesteJAMP.Models;

namespace TesteJAMP.Models
{
    public class PessoaContextDB2 : DbContext
    {
        public DbSet<Pessoa> PessoaBO { get; set; }
    }
}