﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TesteJAMP.Models
{
    public class Pessoa
    {
        public string Nome { get; set; }
        public string DataNascimento { get; set; }
        [Key]
        public string Rg { get; set; }
        public string Sexo { get; set; }
        public string Endereco { get; set; }
        
}
}